export type InputTypeProp =
    | "text"
    | "password"
    | "number"
    | "email"
    | "url"
    | "textarea";

export type InputCounterProp = "off" | "word-limit" | "word-left" | "both";

export type ShapeProp =
    | "circle"
    | "round"
    | "square"
    | "no-border"
    | "round-no-border"
    | "square-no-border"
    | "no-shape";

export type SizeProp =
    | "tiny"
    | "small"
    | "normal"
    | "medium"
    | "large"
    | "xlarge"
    | number;

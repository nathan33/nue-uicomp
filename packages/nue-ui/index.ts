import "@nue-ui/assets";
import type { App } from "vue";
import components from "./components";
import { NueMessage, NueConfirm, NuePrompt } from "../components";

export const installer = {
    install: (app: App) => {
        components.forEach((comp) => {
            app.component(comp.name as string, comp);
        });
    },
};

export default installer;
export { NueMessage as nueMsg, NueConfirm as nueCfm, NuePrompt as nuePrm };

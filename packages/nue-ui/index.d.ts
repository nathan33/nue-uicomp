import { installer } from "./index";
import { NueMessage, NueConfirm, NuePrompt } from "../components";

declare module "nue-ui" {
    export default installer;
    export { NueMessage as nueMsg, NueConfirm as nueCfm, NuePrompt as nuePrm };
}

import NuePopupWrapper from "./src/popup-wrapper.vue";
import { usePopupWrapper, createPopupWrapper } from "./src/popup-wrapper";
import type { popupWrapperFunctions } from "./src/popup-wrapper";

export { NuePopupWrapper, usePopupWrapper, createPopupWrapper };
export type { popupWrapperFunctions };

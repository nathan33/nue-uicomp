import NueHeader from "./src/header.vue";
import NueContainer from "./src/container.vue";
import NueMain from "./src/main.vue";
import NueFooter from "./src/footer.vue";

export { NueContainer, NueHeader, NueMain, NueFooter };

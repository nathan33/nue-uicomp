import { createRouter, createWebHistory, type RouteRecord } from "vue-router";
import type { RouteRecordRaw } from "vue-router";

const docs = import.meta.glob("../views/docs/*.vue");
const docsRoutes: RouteRecordRaw[] = [];
Object.keys(docs).forEach((key) => {
    const prefix = "/components";
    const routeName = key
        .replace("../views/docs/", "")
        .replace(".vue", "")
        .replace("-test", "");
    const route = {
        path: prefix + "/" + routeName,
        name: routeName,
        component: docs[key],
    };
    docsRoutes.push(route);
});

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/",
            name: "index",
            component: () => import("../views/index.vue"),
            children: [
                // {
                //     path: "/todo",
                //     name: "todo",
                //     component: () => import("../views/todo/index.vue"),
                // },
                {
                    path: "/components",
                    name: "components",
                    redirect: "/components/text",
                    children: docsRoutes,
                },
            ],
        },
    ],
});

export default router;

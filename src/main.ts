import { createApp } from "vue";

import App from "./App.vue";
import router from "./router";

import NueUI from "nue-ui";

const app = createApp(App);

app.use(NueUI);
app.use(router);

app.mount("#app");

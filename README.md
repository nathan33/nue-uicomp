# NueUIComp

This is a UI component library for Vue.js.

## Install

```
npm install nue-ui
```

## Usage

```
import {createApp} from 'vue'

import NueUI from 'nue-ui'
import 'nue-ui/style.css'

const app = createApp(App)
app.use(NueUI)
app.mount('#app')
```

## Documentation

[http://nueui.nathan33.xyz/](http://nueui.nathan33.xyz/)

## Gitee

[https://gitee.com/nathan33/nue-uicomp](https://gitee.com/nathan33/nue-uicomp)

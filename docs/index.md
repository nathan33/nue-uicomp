---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
    name: "NueUI"
    text: "一个基于 Vue3 的 UI 组件库"
    tagline: "组件设计灵感基于 Element-Plus 组件库"
    actions:
        - theme: brand
          text: 文档
          link: /pages/introduction
        - theme: alt
          text: 如何使用?
          link: /pages/usage

features:
    - icon: 🛠️
      title: Simple and minimal, always
      details: Lorem ipsum...
    - icon: 🛠️
      title: Another cool feature
      details: Lorem ipsum...
    - icon: 🛠️
      title: Another cool feature
      details: Lorem ipsum...
---
